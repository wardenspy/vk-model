package ru.warden.vkspy;


import java.util.List;

// Here different relationships are stored
public class RelationShips {
  List<String> grandParentsIds;
  List<String> parentsIds;
  List<String> siblingIds;
  List<String> grandChildrenIds;
}
