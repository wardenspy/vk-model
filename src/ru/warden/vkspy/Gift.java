package ru.warden.vkspy;

import java.util.Date;

public class Gift {
    private Long giftId;
    private Long fromId;
    private String message;
    private Date date;

    /*
    0 - everything to everyone
    1 - name to everyone, message to owner
    2 - name is hidden, message to owner
     */
    private int privacy;

    static class GiftInstance {
        private Long id;
        private String thumb_256; //url to 256x256
        private String thumb_96; //url to 96x96
        private String thumb_48; //url to 48x48
    }

}
