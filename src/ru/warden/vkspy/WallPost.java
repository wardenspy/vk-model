package ru.warden.vkspy;

import java.util.Date;
import java.util.List;

public class WallPost {

    private Long postId;
    private Date postDate;
    private boolean isAvailable;
    private boolean isRepost;
    private boolean isPublic;

    private String text;
    private List<Attachment> attachmentList;
    private Long sourceId; // Original post id (if it's a repost)

    private List<Long> likedBy;
    private List<Comment> comments;

    public WallPost(int postId) {

    }

    public void updateState() {
        //todo: обновить список лайков и доступность поста
    }

    public Long getPostId() {
        return postId;
    }

    public boolean isAvailable() {
        //todo: залезть в вк и заново проверить доступность поста
        return isAvailable;
    }

    public boolean isRepost() {
        return isRepost;
    }

    public String getText() {
        return text;
    }

    public List<Attachment> getAttachmentList() {
        return attachmentList;
    }

    public Long getSourceId() {
        return sourceId;
    }
}