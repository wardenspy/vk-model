package ru.warden.vkspy;

public class Audio {

    public final AttachmentType TYPE = AttachmentType.AUDIO;

    private Long id;
    private Long ownerId;
    private String artist;
    private String title;
    private Integer duration;
    private String url;
    private Integer lyricsId;
}
