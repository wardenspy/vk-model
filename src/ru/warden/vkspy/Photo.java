package ru.warden.vkspy;

import java.util.Date;
import java.util.List;

public class Photo implements Attachment {

    public final AttachmentType TYPE = AttachmentType.PHOTO;

    //Photos are available in different sizes
    private static class PhotoCopy {
        private String type; //see vk.com/dev/photo_sizes
        private String url;
        private int width;
        private int height;
    }

    private Long id;
    private Long albumId;
    private Long ownerId;
    private Long userId; //who uploaded (for communities)
    private String text;
    private Date date;

    //Copies of image of different sizes
    private PhotoCopy[] sizes;

    private List<Comment> comments;

}
