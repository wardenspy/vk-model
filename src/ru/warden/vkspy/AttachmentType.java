package ru.warden.vkspy;

public enum AttachmentType {

    PHOTO,
    AUDIO,
    VIDEO,
    POLL,
    ARTICLE,
    DOC,
    LINK,
    GRAFFITI,
    PAGE, //wiki pages
    MARKET,
    MARKET_ALBUM,
    ALBUM,
    STICKER



}