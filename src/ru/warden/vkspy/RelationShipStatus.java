package ru.warden.vkspy;

public enum RelationShipStatus {

  SINGLE,
  IN_RELATIONSHIP,
  ENGAGED
}
