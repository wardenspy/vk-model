package ru.warden.vkspy;

public class UserRelationShipStatus {
  private String relationUserIdentifier;
  private RelationShipStatus status;

  public UserRelationShipStatus(String relationUserIdentifier,
      RelationShipStatus status) {
    this.relationUserIdentifier = relationUserIdentifier;
    this.status = status;
  }

  UserRelationShipStatus(String relationUserIdentifier) {
    this.relationUserIdentifier = relationUserIdentifier;
  }
}
