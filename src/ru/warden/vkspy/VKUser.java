package ru.warden.vkspy;

import java.util.Date;
import java.util.List;

public class VKUser {
  private Long userId;
  private String userLinkId;

  private String status;

  private String firstName;
  private String secondName;
  private String thirdName;

  private UserSex sex;
  private Date birthDate;

  private String hometown;
  private String languages;

  private UserRelationShipStatus relationShipStatus;

  private String workingPlaceCommunityId;
  private String webSite;

  private RelationShips relations;

  private String colledgeOrUniversity; // The place (college or university) where VKUser studies(-ed)
  private String school; // The school where VKUser studies(-ed)

  private List<Gift> gifts;
}
